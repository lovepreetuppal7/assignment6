package com.company;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

public class E5_15 {
    /**
     * The program reads java source file
     * and prints add the identifiers 
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException
    {
        Map<String, ArrayList<Integer>> map = new TreeMap<String, ArrayList<Integer>>();
        String file = "C:\\Users\\lovepreetuppal\\IdeaProjects\\Assignment6\\stc\\E5_15";
        Scanner in = new Scanner(new File(file));
        in.useDelimiter("[^A-Za-z0-9_]+");
        while (in.hasNext()) {
            String line = in.next();
            line = line.trim();
            map.put(line, new ArrayList<Integer>());
        }

        int index = 0;
        for (String line : map.keySet()) {
            // scans the first word
            ArrayList<Integer> finalList = new ArrayList<Integer>();
            Scanner scanner = new Scanner(new File(file));
            in.useDelimiter("[^A-Za-z0-9_]+");
            int lineCount = 0;

            // printing the values
            while(scanner.hasNextLine()) {
                // scan each token in map
                String s = scanner.nextLine();
                lineCount++;
                if(s.contains(line)){
                    finalList.add(lineCount);
                }
                map.put(line, finalList);
            }
            index++;
            System.out.println(index + ": " + line + " occurs in:" + map.get(line));
            System.out.println("");
        }
    }
}


