package com.company;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class E15_4 {
    /**
     * The main method contains all the code required to run the program
     * It allows user to select option between adding and removing students
     * modify and print grades
     */
    public static void main(String[] args) {
        // Define map as assigning value as grade and key as name
        Map<String, String> studentMap = new TreeMap<>();
        Scanner keyboard = new Scanner(System.in);
        // print options to user
        do {
            System.out.println("Select an option");
            System.out.println("      (a)  Add student");
            System.out.println("      (r)  to remove a student");
            System.out.println("      (m)  to modify a grade");
            System.out.println("      (p)  Print all grades");
            System.out.println("      (q)  to quit");
            String option = keyboard.nextLine();
            //keyboard.nextLine();

            // user selects one of the option
            switch (option) {
                // code to add a student in map
                case "a":
                    System.out.println("Enter student name:");
                    String studentName = keyboard.nextLine();
                    if (studentMap.containsKey(studentName)) {
                        System.out.println("Student name already present.");
                        continue;
                    }
                    System.out.println("Enter student grade:");
                    String grade = keyboard.nextLine();
                    studentMap.put(studentName, grade);
                    break;

                    //// code to remove a student in map
                case "r":
                    System.out.println("Enter student name:");
                    studentName = keyboard.nextLine();
                    if (!studentMap.containsKey(studentName)) {
                        System.out.println("Student name not present in map to delete.");
                        continue;
                    }
                    studentMap.remove(studentName);
                    break;

                    // Code to modify students grade
                case "m":
                    System.out.println("Enter student name:");
                    studentName = keyboard.nextLine();
                    if (!studentMap.containsKey(studentName)) {
                        System.out.println("Student name not present in map to modify grade.");
                        continue;
                    }

                    System.out.println("Enter student grade:");
                    grade = keyboard.nextLine();
                    studentMap.put(studentName, grade);
                    break;

                    // prints all names and grade
                case "p" :
                    for (Map.Entry<String, String> entry : studentMap.entrySet()) {
                        System.out.println(entry.getKey() + ":" + entry.getValue());
                    }
                    break;

                    // quits the program
                case "q" :
                System.out.println("You have selected to Exit. Bye");
                System.exit(0);
            }
        } while (true);
    }
}

