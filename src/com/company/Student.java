package com.company;
import java.util.*;

/**
 * student class provides students first and last name
 *  along with the ID numbers
 */
public class Student {
    private int ID;
    private String studentFirstName;
    private String studentLastName;
    public Student(int iD, String studentFirstName, String studentLastName) {
        ID = iD;
        this.studentFirstName = studentFirstName;
        this.studentLastName = studentLastName;
    }
    // getters and setter for public class student
    public int getID() {
        return ID;
    }
    public void setID(int iD) {
        ID = iD;
    }
    public String getFirstName() {
        return studentFirstName;
    }
    public void setFirstName(String firstName) {
        this.studentFirstName = firstName;
    }
    public String getLastName() {
        return studentLastName;
    }
    public void setLastName(String lastName) {
        this.studentLastName = lastName;
    }

    /**
     *
     * Override methods for equal, hashCode, and toString
     *
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        if (ID != student.getID()) return false;
        if (studentFirstName != null ? !studentFirstName.equals(student.getFirstName()) : student.getFirstName() != null) return false;
        return studentLastName != null ? studentLastName.equals(student.getLastName()) : student.getLastName() == null;
    }
    @Override
    public int hashCode() {
        //return Object.hash(firstName, lastName, ID);
        int result = (int) (ID ^ (ID >>> 32));
        result = 31 * result + (studentFirstName != null ? studentFirstName.hashCode() : 0);
        result = 31 * result + (studentLastName != null ? studentLastName.hashCode() : 0);
        return result;
    }
    public String toString() {
        return studentFirstName + " " + studentLastName + " (ID = " + ID + ") ";
    }
}

