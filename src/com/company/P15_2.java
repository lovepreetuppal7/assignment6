package com.company;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Scanner;

public class P15_2 {
    public static void main(String[] args) {
         // Define map as assigning value as grade and key as name
        Map<Student, String> studentMap = new TreeMap<>(new StudentComparator());
        Scanner keyboard = new Scanner(System.in);
        // print options to user
        do {
            System.out.println("Select an option");
            System.out.println("      (a)  Add student");
            System.out.println("      (r)  to remove a student");
            System.out.println("      (m)  to modify a grade");
            System.out.println("      (p)  Print all grades");
            System.out.println("      (q)  to quit");
            String option = keyboard.nextLine();
            switch (option) {
                // checks to see if a students is already present, if not then adds a new student
                case "a":
                    boolean isPresent = false;
                    System.out.print("Enter student ID:");
                    int studentID = Integer.parseInt(keyboard.nextLine());
                    for (Map.Entry<Student, String> entry : studentMap.entrySet()) {
                        Student s = entry.getKey();
                        if (s.getID() == studentID) {
                            System.out.println("Student already present.");
                            isPresent = true;
                        }
                    }
                    if (!isPresent) {
                        System.out.print("Enter student's first name:");
                        String studentFirstName = keyboard.nextLine();
                        System.out.print("Enter student's last name:");
                        String studentLastName = keyboard.nextLine();
                        System.out.print("Enter student grade:");
                        String grade = keyboard.nextLine();
                        Student student = new Student(studentID, studentFirstName, studentLastName);
                        studentMap.put(student, grade);
                    }

                    break;
                case "r":
                    boolean isRemoved = false;
                    System.out.println("Enter student ID:");
                    int ID = Integer.parseInt(keyboard.nextLine());
                    //studentID = keyboard.nextInt();

                    for (Map.Entry<Student, String> entry : studentMap.entrySet()) {
                        Student s = entry.getKey();
                        if (s.getID() == ID) {
                            studentMap.remove(s);
                            isRemoved = true;
                        }

                    }
                    if (!isRemoved) {
                        System.out.println("Student ID not present in map to delete.");
                        continue;
                    }
                    break;

                case "m":
                    // modify students grade
                    boolean isModified = false;
                    System.out.println("Enter student ID:");
                    ID = Integer.parseInt(keyboard.nextLine());
                    for (Map.Entry<Student, String> entry : studentMap.entrySet()) {
                        Student s = entry.getKey();
                        if (s.getID() == ID) {
                            System.out.println("Enter student grade:");
                            String grade = keyboard.nextLine();
                            studentMap.put(s, grade);
                            isModified = true;
                        }
                    }
                    if (!isModified) {
                        System.out.println("Student not present in map to modify grade.");
                        continue;
                    }
                    //System.out.println("Enter student grade:");
                    //grade = keyboard.nextLine();
                    //studentMap.put(studentName, grade);
                    break;
                case "p":
                    for (Map.Entry<Student, String> entry : studentMap.entrySet()) {
                        System.out.println(entry.getKey() + ":" + entry.getValue());
                    }
                    break;
                case "q":
                    System.out.println("You have selected to Exit. Bye");
                    System.exit(0);
            }
        } while (true);
    }
// sorts students in map according to last name, first name, then id number
    static class StudentComparator implements Comparator<Student> {
        @Override
        public int compare(Student s1, Student s2) {
            if (s1.getLastName().compareTo(s2.getLastName()) > 0)
                return 1;
            else if (s1.getLastName().compareTo(s2.getLastName()) < 0)
                return -1;
            else {
                if (s1.getFirstName().compareTo(s2.getFirstName()) > 0)
                    return 1;
                else if (s1.getFirstName().compareTo(s2.getFirstName()) < 0)
                    return -1;
                else {
                    if (s1.getID() > s2.getID())
                        return 1;
                    else if (s1.getID() < s2.getID())
                        return -1;
                    else
                        return 0;
                }
            }
        }
    }
}